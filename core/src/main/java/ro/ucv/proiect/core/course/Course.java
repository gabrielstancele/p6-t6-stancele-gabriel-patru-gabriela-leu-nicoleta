package ro.ucv.proiect.core.course;

import ro.ucv.proiect.core.user.User;

import java.io.File;
import java.util.Set;

public class Course {

    private final int id;
    private final User professor;
    private final String name;
    private final File banner;
    private final Set<User> students;

    public Course(int id, User professor, String name, File banner, Set<User> students) {
        this.id = id;
        this.professor = professor;
        this.name = name;
        this.banner = banner;
        this.students = students;
    }

    public User getProfessor() {
        return professor;
    }

    public String getName() {
        return name;
    }

    public File getBanner() {
        return banner;
    }

    public Set<User> getStudents() {
        return students;
    }

}
