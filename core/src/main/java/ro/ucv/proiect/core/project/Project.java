package ro.ucv.proiect.core.project;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

public class Project {

    private final int id;
    private final int owner;
    private String name;
    private String description;
    private final Timestamp startDate;
    private Timestamp endDate;
    private int points;
    private final Set<String> acceptedExtensions;
    private final Set<Submission> submissions = new HashSet<>();

    public Project(int id, int owner, String name, String description, Timestamp startDate, Timestamp endDate, int points, Set<String> acceptedExtensions) {
        this.id = id;
        this.owner = owner;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.points = points;
        this.acceptedExtensions = acceptedExtensions;
    }

    public int getId() {
        return id;
    }

    public int getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Set<String> getAcceptedExtensions() {
        return acceptedExtensions;
    }

    public void addSubmission(final Submission submission) {
        this.submissions.add(submission);
    }

}
