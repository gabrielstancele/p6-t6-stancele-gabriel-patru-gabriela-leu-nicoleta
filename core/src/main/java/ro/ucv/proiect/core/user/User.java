package ro.ucv.proiect.core.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class User {

    private final int id;
    private final Role role;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final byte[] password;

    public User(int id, Role role, String firstName, String lastName, String email, byte[] password) {
        this.id = id;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    /**
     * Creaza un {@link User} dintr-un {@link ResultSet} in urma unui {@link PreparedStatement#executeQuery() query}
     * de tip {@code SELECT}
     *
     * @param resultSet resultSet
     * @return un {@link User} daca datele din {@link ResultSet} au putut fi parsate corect, altfel, o valoare nula
     * @throws SQLException if the columnLabel is not valid; if a database access error occurs or this method is called
     *                      on a closed result set (copiat din {@link ResultSet#getInt(int)})
     */
    public static User from(final ResultSet resultSet) throws SQLException {
        return new User(
                resultSet.getInt("id"),
                Role.getById(resultSet.getInt("role")),
                resultSet.getString("first_name"),
                resultSet.getString("last_name"),
                resultSet.getString("email"),
                resultSet.getBytes("password")
        );
    }

    public int getId() {
        return id;
    }

    public Role getRole() {
        return role;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    /**
     * Returneaza emailul impreuna cu domeniul rolului userului
     *
     * @return un {@link String} de forma {@code email@domain}
     * @see Role#getDomain()
     */
    public String getEmail() {
        return "%s@%s".formatted(email, role.getDomain());
    }

    public byte[] getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "User{" +
                "role=" + role +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + getEmail() + '\'' +
                '}';
    }
}
