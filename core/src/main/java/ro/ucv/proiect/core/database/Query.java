package ro.ucv.proiect.core.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

enum Query {

    INSERT_USER,

    SELECT_USER_BY_ID,
    SELECT_USER_BY_EMAIL;

    private final String query;

    Query() {
        String temp = null;
        final var parts = name().toLowerCase().split("_", 2);

        try (final var in = Query.class.getResourceAsStream("/sql/%s/%s.sql".formatted(parts[0], parts[1]))) {
            temp = new String(in.readAllBytes());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }

        this.query = temp;
    }

    public PreparedStatement prepare(final Connection connection) throws SQLException {
        return connection.prepareStatement(this.query);
    }

}
