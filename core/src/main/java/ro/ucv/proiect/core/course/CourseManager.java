package ro.ucv.proiect.core.course;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

public class CourseManager {

    private final LoadingCache<Integer, Course> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(Duration.ofMinutes(5))
            .build(CacheLoader.from(i -> null));

    public Course getCourse(final int id) {
        try {
            return cache.get(id);
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

}
