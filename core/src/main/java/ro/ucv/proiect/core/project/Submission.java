package ro.ucv.proiect.core.project;

import ro.ucv.proiect.core.user.User;

import java.io.File;
import java.sql.Timestamp;

public class Submission {

    private final Project project;
    private final User student;
    private final File file;
    private final Timestamp date;

    public Submission(Project project, User student, File file, Timestamp date) {
        this.project = project;
        this.student = student;
        this.file = file;
        this.date = date;
    }

    public Project getProject() {
        return project;
    }

    public User getStudent() {
        return student;
    }

    public File getFile() {
        return file;
    }

    public Timestamp getDate() {
        return date;
    }

}
