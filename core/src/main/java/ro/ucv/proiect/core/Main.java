package ro.ucv.proiect.core;

import ro.ucv.proiect.core.database.DatabaseManager;
import ro.ucv.proiect.core.user.UserManager;

/**
 * Temporary class
 */
public class Main {

    public static void main(String[] args) {
        final var databaseManager = new DatabaseManager();
        final var userManager = new UserManager(databaseManager);
    }

}
