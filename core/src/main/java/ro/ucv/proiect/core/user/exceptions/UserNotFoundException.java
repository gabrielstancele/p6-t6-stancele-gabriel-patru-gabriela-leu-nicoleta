package ro.ucv.proiect.core.user.exceptions;

/**
 * Exceptie ridicata cand un {@link ro.ucv.proiect.core.user.User} nu a putut fi gasit
 *
 * @see ro.ucv.proiect.core.database.DatabaseManager#getUserById(int)
 */
public class UserNotFoundException extends RuntimeException {

    private UserNotFoundException(String message) {
        super(message);
    }

    public static UserNotFoundException byId(final int id) {
        return new UserNotFoundException("User with id " + id + " not found.");
    }

    public static UserNotFoundException byEmail(final String email) {
        return new UserNotFoundException("User with email " + email + " not found.");
    }

}
