package ro.ucv.proiect.core.user;

import ro.ucv.proiect.core.util.Constant;

public enum Role {

    PROFESSOR(Constant.PROFESSOR_ROLE_ID, Constant.PROFESSOR_EMAIL_DOMAIN),
    STUDENT(Constant.STUDENT_ROLE_ID, Constant.STUDENT_EMAIL_DOMAIN);

    private final int id;
    private final String domain;

    Role(int id, String domain) {
        this.id = id;
        this.domain = domain;
    }

    public static Role getById(final int id) {
        return switch (id) {
            case Constant.PROFESSOR_ROLE_ID -> PROFESSOR;
            case Constant.STUDENT_ROLE_ID -> STUDENT;
            default -> throw new IllegalArgumentException("Unknown role id " + id);
        };
    }

    public static Role getRoleByEmail(final String email) {
        if (email.equals(PROFESSOR.getDomain())) {
            return PROFESSOR;
        }

        if (email.equals(STUDENT.getDomain())) {
            return STUDENT;
        }

        return null;
    }

    public int getId() {
        return id;
    }

    public String getDomain() {
        return domain;
    }

}
