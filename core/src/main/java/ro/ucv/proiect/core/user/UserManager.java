package ro.ucv.proiect.core.user;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import ro.ucv.proiect.core.database.DatabaseManager;

import java.time.Duration;
import java.util.concurrent.ExecutionException;

public class UserManager {

    private final LoadingCache<Integer, User> userCacheById;
    private final LoadingCache<String, User> userCacheByEmail;

    public UserManager(DatabaseManager databaseManager) {
        this.userCacheById = CacheBuilder.newBuilder()
                .expireAfterWrite(Duration.ofMinutes(5))
                .build(CacheLoader.from(databaseManager::getUserById));
        this.userCacheByEmail = CacheBuilder.newBuilder()
                .expireAfterWrite(Duration.ofMinutes(5))
                .build(CacheLoader.from(databaseManager::getUserByEmail));
    }

    public User getUser(final int id) {
        try {
            return userCacheById.get(id);
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public User getUser(final String email) {
        try {
            return userCacheByEmail.get(email);
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateUser(final User user) {

    }

}
