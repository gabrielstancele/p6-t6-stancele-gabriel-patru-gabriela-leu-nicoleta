package ro.ucv.proiect.core.util;

public final class Constant {

    public static final int PROFESSOR_ROLE_ID = 1;
    public static final int STUDENT_ROLE_ID = 2;
    public static final String PROFESSOR_EMAIL_DOMAIN = "profesor.ucv.ro";
    public static final String STUDENT_EMAIL_DOMAIN = "student.ucv.ro";

}
