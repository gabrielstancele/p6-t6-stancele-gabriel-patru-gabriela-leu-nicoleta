package ro.ucv.proiect.core.user;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

@SuppressWarnings("SpellCheckingInspection")
public class PasswordProcessor {

    private final SecureRandom random = new SecureRandom();
    private final SecretKeyFactory secretKeyFactory;

    public PasswordProcessor() {
        SecretKeyFactory temp = null;

        try {
            temp = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        this.secretKeyFactory = temp;
    }

    private static final class Instance {
        private static final PasswordProcessor instance = new PasswordProcessor();
    }

    public static PasswordProcessor get() {
        return Instance.instance;
    }

    /**
     * Metoda folosita pentru a cripta parole folosind algoritmul {@code PBKDF2WithHmacSHA1}
     *
     * @param password parola de criptat
     * @return parola criptata sub forma unui array de {@link Byte}
     * @throws InvalidKeySpecException datorita {@link SecretKeyFactory#generateSecret(KeySpec)}
     */
    public byte[] encode(final String password) throws InvalidKeySpecException {
        final var salt = new byte[16];
        random.nextBytes(salt);
        return secretKeyFactory.generateSecret(new PBEKeySpec(password.toCharArray(), salt, 2022, 128)).getEncoded();
    }

    /**
     * Verifica daca o parola necriptata este egala cu cea criptata
     *
     * @param password        parola necriptat
     * @param encodedPassword parola criptata
     * @return data cele doua parole sunt egale
     */
    public boolean isCorrect(final String password, byte[] encodedPassword) {
        try {
            return Arrays.equals(encode(password), encodedPassword);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            return false;
        }
    }

}
