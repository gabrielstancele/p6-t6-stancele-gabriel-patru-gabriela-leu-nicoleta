package ro.ucv.proiect.core.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ro.ucv.proiect.core.user.PasswordProcessor;
import ro.ucv.proiect.core.user.Role;
import ro.ucv.proiect.core.user.User;
import ro.ucv.proiect.core.user.exceptions.UserNotFoundException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.function.Function;

public class DatabaseManager {

    private HikariDataSource dataSource;
    private Connection connection;

    public DatabaseManager() {
        connect();
        createTables();
    }

    private void connect() {
        try (final var in = getClass().getResourceAsStream("/sql/hikari.properties")) {
            final var properties = new Properties();
            properties.load(in);
            this.dataSource = new HikariDataSource(new HikariConfig(properties));

            if (this.dataSource.isRunning()) {
                this.connection = this.dataSource.getConnection();
            }
        } catch (IOException | SQLException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    private void createTables() {
        final var scripts = List.of(
                "roles",
                "users",
                "projects",
                "submissions",
                "courses",
                "enrolments"
        );

        try {
            for (final var script : scripts) {
                final var in = getClass().getResourceAsStream("/sql/tables/" + script + ".sql");
                final var query = new String(in.readAllBytes());
                in.close();
                this.connection.prepareStatement(query).executeUpdate();
            }
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    private <T> User findUser(final PreparedStatement statement, final T t, final Function<T, UserNotFoundException> exceptionFunction) throws SQLException {
        try (final var result = statement.executeQuery()) {
            if (result.first()) {
                return User.from(result);
            }

            throw exceptionFunction.apply(t);
        }
    }

    /**
     * Get a {@link User} by its {@code  id}
     *
     * @param id user's id
     * @return {@link User} if found, otherwise null
     * @throws UserNotFoundException if a user with the provided id was not found
     */
    public User getUserById(final int id) {
        try (final var statement = Query.SELECT_USER_BY_ID.prepare(this.connection)) {
            statement.setInt(1, id);
            return findUser(statement, id, UserNotFoundException::byId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public User getUserByEmail(final String email) {
        try (final var statement = Query.SELECT_USER_BY_EMAIL.prepare(this.connection)) {
            statement.setString(1, email);
            return findUser(statement, email, UserNotFoundException::byEmail);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void createUser(final String firstName, final String lastName, final String email, final String password, final Role role, final File avatar) {
        try (
                final var statement = Query.INSERT_USER.prepare(this.connection);
                final var avatarStream = avatar == null ? null : new FileInputStream(avatar);
        ) {
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, email.toLowerCase());
            statement.setBytes(4, PasswordProcessor.get().encode(password));
            statement.setInt(5, role.getId());
            statement.setBlob(6, avatarStream);
            statement.executeUpdate();
        } catch (InvalidKeySpecException | IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUser(final User user) {
    }

}
