module proiect.core.main {
    requires com.google.common;
    requires com.zaxxer.hikari;
    requires java.sql;

    exports ro.ucv.proiect.core.user to proiect.app.main;
    exports ro.ucv.proiect.core.database to proiect.app.main;

    opens ro.ucv.proiect.core.database to com.zaxxer.hikari;
}