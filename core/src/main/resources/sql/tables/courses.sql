CREATE TABLE IF NOT EXISTS `COURSES`
(
    id        INT PRIMARY KEY AUTO_INCREMENT,
    professor INT         NOT NULL,
    name      VARCHAR(50) NOT NULL,
    banner    MEDIUMBLOB  NULL,

    FOREIGN KEY (professor) REFERENCES USERS (id)
);