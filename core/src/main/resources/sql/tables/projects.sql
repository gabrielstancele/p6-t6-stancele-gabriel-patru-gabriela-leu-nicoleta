CREATE TABLE IF NOT EXISTS `PROJECTS`
(
    id                  INT PRIMARY KEY AUTO_INCREMENT,
    owner               INT          NOT NULL,
    name                VARCHAR(50)  NOT NULL,
    description         VARCHAR(250) NOT NULL,
    start_date          TIMESTAMP    NOT NULL,
    end_date            TIMESTAMP    NOT NULL,
    points              INT UNSIGNED NOT NULL,
    accepted_extensions VARCHAR(100) NOT NULL, -- comma separated list of extensions: pdf,docx,zip

    FOREIGN KEY (owner) REFERENCES USERS (id)
);