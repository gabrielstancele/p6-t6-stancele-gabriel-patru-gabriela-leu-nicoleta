CREATE TABLE IF NOT EXISTS `USERS`
(
    id         INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50)    NOT NULL,
    last_name  VARCHAR(50)    NOT NULL,
    email      VARCHAR(50)    NOT NULL,
    password   VARBINARY(128) NOT NULL,
    role       INT            NOT NULL,
    avatar     MEDIUMBLOB     NULL,

    CONSTRAINT unique_email UNIQUE (email),
    FOREIGN KEY (role) REFERENCES ROLES (id)
);