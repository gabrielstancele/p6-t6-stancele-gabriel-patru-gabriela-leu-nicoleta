CREATE TABLE IF NOT EXISTS `ENROLMENTS`
(
    student INT NOT NULL,
    course  INT NOT NULL,

    CONSTRAINT unique_student_and_course_combination UNIQUE (student, course),
    FOREIGN KEY (student) REFERENCES USERS (id) ON DELETE CASCADE,
    FOREIGN KEY (course) REFERENCES COURSES (id) ON DELETE CASCADE
);