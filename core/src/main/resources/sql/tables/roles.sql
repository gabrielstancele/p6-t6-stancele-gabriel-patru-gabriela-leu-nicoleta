CREATE TABLE IF NOT EXISTS `ROLES`
(
    id   INT PRIMARY KEY,
    name VARCHAR(25) NOT NULL
);

INSERT IGNORE INTO `ROLES` (id, name)
VALUES (1, 'professor'),
       (2, 'student');