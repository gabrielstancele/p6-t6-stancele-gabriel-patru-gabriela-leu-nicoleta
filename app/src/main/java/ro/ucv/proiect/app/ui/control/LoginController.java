package ro.ucv.proiect.app.ui.control;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class LoginController {

    public TextField emailInput;
    public PasswordField passwordInput;
    public Button loginButton;

    @FXML
    public void onLoginButtonClick(MouseEvent actionEvent) {
        final var email = emailInput.getCharacters().toString();
        final var password = passwordInput.getCharacters().toString();

        System.out.println(email);
        System.out.println(password);
    }
}
