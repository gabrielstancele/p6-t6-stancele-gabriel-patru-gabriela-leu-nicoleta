package ro.ucv.proiect.app.login;

import ro.ucv.proiect.core.user.PasswordProcessor;
import ro.ucv.proiect.core.user.User;
import ro.ucv.proiect.core.user.UserManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("SpellCheckingInspection")
public class LoginHandler {

    // Format: nume.prenum@profesor.ucv.ro
    private static final Pattern PROFESSOR_EMAIL_PATTERN = Pattern.compile("(?<localPart>\\w+\\.\\w+)@(?<domain>profesor\\.ucv\\.ro)");
    // Format: nume.prenume.cod@student.ucv.ro
    private static final Pattern STUDENT_EMAIL_PATTERN = Pattern.compile("(?<localPart>\\w+\\.\\w+\\.\\w+)@(?<domain>student\\.ucv\\.ro)");
    private static final Pattern PASSWORD_PATTERN = Pattern.compile("[\\w]{8,40}");

    private final UserManager userManager;
    private User connectedUser;

    public LoginHandler(UserManager userManager) {
        this.userManager = userManager;
    }

    private Matcher createMatcherForEmail(final String email) {
        final var professorEmailMatcher = PROFESSOR_EMAIL_PATTERN.matcher(email);

        // Emailul este unul de profesor
        if (professorEmailMatcher.matches()) {
            return professorEmailMatcher;
        }

        final var studentEmailMatcher = STUDENT_EMAIL_PATTERN.matcher(email);

        // Emailul este unul de student
        if (studentEmailMatcher.matches()) {
            return studentEmailMatcher;
        }

        return null;
    }

    private boolean isValidPassword(final String password) {
        return PASSWORD_PATTERN.matcher(password).matches();
    }

    public User getConnectedUser() {
        return connectedUser;
    }

    public LoginResponse login(final String email, final String password) {
        // Parola nu coresponde cerintelor (prea scurta / lunga, contine caractere neadmise, etc.)
        if (!isValidPassword(password)) {
            return LoginResponse.of(LoginResponse.Response.INVALID_PASSWORD);
        }

        final var emailMatcher = createMatcherForEmail(email);

        // Emailul nu este unul de profesor sau student
        if (emailMatcher == null) {
            return LoginResponse.of(LoginResponse.Response.INVALID_EMAIL);
        }

        //final var role = Role.getRoleByEmail(emailMatcher.group("domain").toLowerCase());
        final var user = userManager.getUser(email);

        // Un user cu acest email nu a fost gasit
        if (user == null) {
            return LoginResponse.of(LoginResponse.Response.USER_NOT_FOUND);
        }

        // Parola introdusa este cea a contului
        if (PasswordProcessor.get().isCorrect(password, user.getPassword())) {
            this.connectedUser = user;
            return LoginResponse.of(LoginResponse.Response.OK, user);
        }

        // Parona introdusa nu este corecta
        return LoginResponse.of(LoginResponse.Response.INCORRECT_PASSWORD);
    }

}
