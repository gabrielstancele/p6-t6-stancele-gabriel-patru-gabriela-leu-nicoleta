package ro.ucv.proiect.app.login;

import ro.ucv.proiect.core.user.User;

public class LoginResponse {

    private final Response response;
    private final User user;

    private LoginResponse(Response response, User user) {
        this.response = response;
        this.user = user;
    }

    public static LoginResponse of(final Response response, final User user) {
        return new LoginResponse(response, user);
    }

    public static LoginResponse of(final Response response) {
        return of(response, null);
    }

    public Response getResponse() {
        return response;
    }

    public User getUser() {
        return user;
    }

    public enum Response {

        INCORRECT_PASSWORD,
        INVALID_EMAIL,
        INVALID_PASSWORD,
        OK,
        USER_NOT_FOUND,

    }

}
