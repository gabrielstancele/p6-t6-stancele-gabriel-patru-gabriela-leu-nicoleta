package ro.ucv.proiect.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ro.ucv.proiect.core.database.DatabaseManager;

public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        final var manager = new DatabaseManager();
        final var scene = new Scene(new FXMLLoader(getClass().getClassLoader().getResource("scenes/login.fxml")).load());
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
