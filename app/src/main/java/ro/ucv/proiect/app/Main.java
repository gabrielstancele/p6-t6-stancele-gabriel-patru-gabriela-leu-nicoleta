package ro.ucv.proiect.app;

import ro.ucv.proiect.app.login.LoginHandler;
import ro.ucv.proiect.core.database.DatabaseManager;
import ro.ucv.proiect.core.user.Role;
import ro.ucv.proiect.core.user.UserManager;

public final class Main {

    private Main() {
        final var databaseManager = new DatabaseManager();
        final var userManager = new UserManager(databaseManager);

        final var loginHandler = new LoginHandler(userManager);
    }

    public static void main(String[] args) {
        //Application.launch(App.class, args);
        new DatabaseManager().createUser(
                "Gabriel",
                "Stancele",
                "gabriel.stancele.abcd",
                "!Password",
                Role.STUDENT,
                null
        );
    }

}
