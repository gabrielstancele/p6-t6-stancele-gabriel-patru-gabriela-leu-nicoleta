module proiect.app.main {
    requires proiect.core.main;
    requires javafx.controls;
    requires javafx.fxml;

    opens ro.ucv.proiect.app.ui.control to javafx.fxml;

    exports ro.ucv.proiect.app to javafx.graphics;
}